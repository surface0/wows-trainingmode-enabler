# WoWs Training Mode Enabler

[![MIT License][license-image]][license-url]

## 概要

これはWorld of Warshipsのトレーニングモードを有効化するスクリプトです。  
WoWsのバージョンアップの度に行うXMLファイルの書き換えを自動で行います。
WindowsPCで利用できます。

## 使い方

1. ``wows-traningmode-enabler.js``のファイルアイコンをダブルクリックしてください。
2. 確認ダイアログがでますので、「OK」を選択して完了です。

## WoWsのインストール先をデフォルトの場所以外にしている場合

事前にスクリプト内で指定しているファイルパスを書き換える必要があります。  
テキストエディタなどで``wows-traningmode-enabler.js``を開き、次の箇所を自身のWoWsインストール先にあるscripts_config.xmlのパスに書き換えて保存してください。

```js
var WOWS_SCRIPT_CONFIG_PATH = 'C:\\Games\\World_of_Warships\\res\\scripts_config.xml';
```

## WoWsをバージョンアップしたらこのスクリプトが効かなくなった場合

従来とXMLファイルのフォーマットが変わった可能性があります。  
作者までご一報くださりますと助かります。

[license-image]:http://img.shields.io/badge/license-MIT-000000.svg?style=flat-square
[license-url]:LICENSE.txt
