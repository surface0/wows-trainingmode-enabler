// vim: expandtab ts=4 fenc=cp932 ff=dos ft=js
/**
 * WoWs Training Mode Enabler
 * @see https://bitbucket.org/surface0/wows-trainingmode-enabler
 * @author surface0
 * @version 0.6.6
 */

var WOWS_SCRIPT_CONFIG_PATH = 'C:\\Games\\World_of_Warships\\res\\scripts_config.xml';

var TRAINING_MODE_FLAG_TAG_NAME = 'disableTrainingRoom';

var sh = new ActiveXObject('WScript.Shell');
var ret = sh.Popup('トレーニングモードを有効にします', 0, 'WoWsトレーニングイネーブラ', 1 + 32);

var xml = WScript.CreateObject('MSXML.DOMDocument');
xml.load(WOWS_SCRIPT_CONFIG_PATH);

var nodes = xml.getElementsByTagName(TRAINING_MODE_FLAG_TAG_NAME);
if (nodes) {
    var text = nodes[0].firstChild;
    text.nodeValue = 'false';
} else {
    var node = xml.createElement(TRAINING_MODE_FLAG_TAG_NAME);
    var text = xml.createTextNode('false')
    node.appenChild(text);
    xml.appendChild(node);
}

xml.save(WOWS_SCRIPT_CONFIG_PATH);